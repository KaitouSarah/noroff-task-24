﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Noroff_Task_24.Models;

namespace Noroff_Task_24.Controllers
{
    public static class SupervisorGroup
    {
        //creates the static list of supervisors
        private static List<Supervisor> CurrentSupervisors = new List<Supervisor> {};

        public static List<Supervisor> Supervisors
        {
            get { return CurrentSupervisors; }
        }

        public static void AddSupervisor(Supervisor newSupervisor)
        {
            CurrentSupervisors.Add(newSupervisor);
        }
    }
}
