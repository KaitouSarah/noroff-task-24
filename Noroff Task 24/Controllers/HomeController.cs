﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Noroff_Task_24.Models;

namespace Noroff_Task_24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Time = $"Page loaded at {DateTime.Now.ToString("h:mm:ss tt")}";
            ViewBag.WelcomeMessage = "Welcome to our supervisor managment system";

            return View("MyFirstView");
        }

        [HttpGet]
        public IActionResult AddSupervisor()
        {
            //old code from task 24
            /*            Supervisor supervisor1 = new Supervisor("Dean", false);
                        Supervisor supervisor2 = new Supervisor("Greg", true);
                        Supervisor supervisor3 = new Supervisor("Janne", false);
                        Supervisor supervisor4 = new Supervisor("Lene", true);

                        List<Supervisor> supervisors = new List<Supervisor>();
                        supervisors.Add(supervisor1);
                        supervisors.Add(supervisor2);
                        supervisors.Add(supervisor3);
                        supervisors.Add(supervisor4);

                        return View(supervisors);*/

            //new code from task 25
            return View();
        }

        [HttpPost]
        public IActionResult AddSupervisor(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.AddSupervisor(supervisor);

                return View("ListOfSupervisors", SupervisorGroup.Supervisors);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult ListOfSupervisors(Supervisor supervisor)
        {
            return View(SupervisorGroup.Supervisors);
        }

        [HttpGet]
        public IActionResult TestListOfSupervisors() => View(Supervisor.GetGroupOfSupervisors().Where(s => s.Name.StartsWith("S")));

    }
}
