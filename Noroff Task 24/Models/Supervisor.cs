﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Noroff_Task_24.Models
{
    public class Supervisor
    {
        [Required(ErrorMessage = "Please enter a number")]
        [RegularExpression("^\\d+$", ErrorMessage ="Please enter a valid whole number")]
        public int? Id { get; set; } = 1337;
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "S. Jon \"default\" Doe";
        [Required(ErrorMessage = "Please select availability")]
        public bool? IsAvailable { get; set; } = false;

        public Supervisor(int id, string name, bool isAvailable)
        {
            Id = id;
            Name = name;
            IsAvailable = isAvailable;
        }

        public Supervisor() { }

        //Returns a list of supervisors, where one has default values
        public static List<Supervisor> GetGroupOfSupervisors()
        {
            return new List<Supervisor>{
                new Supervisor(1, "Hetfield", false),
                new Supervisor(2, "Draiman", true),
                new Supervisor(3, "Shadows", true),
                new Supervisor()
            };
        }

    }
}
