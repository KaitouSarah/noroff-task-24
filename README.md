# Noroff Task 24 & Task 25 & 26

---Task 24---
� Create a new ASP.NetCore MVC solution/application 

� It must have a new home page 

� Use the viewbag to display the current time in the home page 

� It must have a page to display information about supervisors(first just one) 

� Upgrade the page to display a list of supervisors 

� Do not use any previously created views/layouts or action methods that comes with the solution

---Task 25---
�Upgrade you PGM to allow for the user to enter and view Supervisors

�Use a temporary data store as per the tutorial

�The form should have full validation

�Use Bootstrap to make it look good - Experiment

---Task 26---
� Upgrade your supervisor class to have the ability to use null
conditional operators. Demonstrate this working any way
you want

� Additionally, include default values for properties in the
supervisor class

� Create a method in Supervisors which generates and
returns a List of 4 supervisors

� Create a view to display this list but only if their name
starts with �S� (use a lambda expression in the controller)